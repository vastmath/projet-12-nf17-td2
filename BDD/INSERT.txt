INSERT INTO Budget VALUES (1,15000),
(2,75000),
(14,100000),
(3,10000);

INSERT INTO LigneBudgetaire VALUES (10000,'Salaire','Fonctionnement',1),
(10000,'Salaire','Fonctionnement',2),
(10000,'Salaire','Fonctionnement',14),
(10000,'Salaire','Fonctionnement',3),
(5000,'Ordinateur','Materiel',1),
(50000,'Van','Materiel',2),
(200,'Blouse','Materiel',2);

INSERT INTO EntiteJuridique VALUES ('ONU'),
('OMS'),
('France'),
('UTC'),
('NASA');

INSERT INTO Financeur VALUES ('ONU','1945-10-24','2020-05-28'),
('OMS','1948-4-7','2020-05-28'),
('UTC','1972-10-2','2020-05-28'),
('France','1792-09-22','2020-05-28'),
('NASA','1958-07-29','2020-05-28');

INSERT INTO EmployeContact VALUES ('PrésidentRepublique','emmanuel.macron@republique.fr','0650407080','France'),
('ChefFusee','responsable.fusee@nasa.com','0650407080','NASA'),
('ChefStationSpatial','responsable.iss@nasa.com','0650407080','NASA'),
('PrésidentUTC','philippe.courtier@utc.fr','0650407080','UTC'),
('Premier Ministre','edouard.philippe@republique.fr','0650407080','France');

INSERT INTO OrganismeDeProjet VALUES ('Laboratoire CNRS1','2000-01-01','100',TRUE),
('Laboratoire CNRS2','2000-01-01','100',TRUE),
('SpaceX','2000-01-01','100',TRUE),
('Star Wars','2000-01-01','100','0');
INSERT INTO OrganismeDeProjet(nom,creation,existe) VALUES ('Laboratoire CNRS3','2000-01-01',TRUE);

INSERT INTO AppelProjet VALUES ('Etablir une colonie sur Mars','Definir le plan de vol parfait jusqu a Mars','2020-05-28',100,'SpaceX'),
('Etablir une colonie sur Mars','Recruter les meilleurs colons pour faire partie du premier vol','2020-05-28',100,'SpaceX'),
('Etablir une colonie sur Mars','Construire un vaisseau capable de réaliser le voyage','2020-05-28',100,'SpaceX'),
('Calculer l incertitude','A l aide de modèles probabilistes, réussir à approximer l incertitude','2020-05-28',100,'Laboratoire CNRS3'),
('Renverser l empire','Former la rébellion','2020-05-28',100,'Star Wars');

INSERT INTO Proposition VALUES ('Utiliser les méthodes de l IA','2020-05-29','2020-06-30',FALSE,3,'Calculer l incertitude','A l aide de modèles probabilistes, réussir à approximer l incertitude'),
('Appeler Han Solo','2020-05-29','2020-06-30',TRUE,1,'Renverser l empire','Former la rébellion');

INSERT INTO Projet VALUES ('Appeler Han Solo','2020-06-30','2021-07-30','Appeler Han Solo'),
('Utiliser les méthodes de l IA','2020-06-30','2021-07-30','Utiliser les méthodes de l IA');

INSERT INTO Membre VALUES ('Einstein','Scientifique','albert.einstein@recherche.com'),
('Vast','Etudiant','mathias.vast@etu.fr'),
('Denoeux','Chercheur','thierry.denoeux@recherche.com'),
('Julia','Chercheur','luc.julia@recherche.com'),
('Hawking','Scientifique','stephen.hawking@recherche.com'),
('Carrez','Etudiant','emilien.carrez@etu.fr');

INSERT INTO Interne (nom,fonction,specialite) VALUES
('Vast','Etudiant','Informatique'),
('Carrez','Etudiant','Informatique');
INSERT INTO Interne (nom,fonction,quotite,etablissement) VALUES
('Denoeux','Chercheur',45,'Université de Technologie de Compiègne');

INSERT INTO Externe VALUES ('Einstein','Scientifique'),
('Julia','Chercheur'),
('Hawking','Scientifique');

INSERT INTO Depense VALUES ('2020-08-15',500,'Materiel','Appeler Han Solo','Vast','Etudiant','Denoeux','Chercheur'),
('2020-08-20',1000,'Fonctionnement','Appeler Han Solo','Vast','Etudiant','Einstein','Scientifique');

INSERT INTO MembreContribueAProjet VALUES ('Vast','Etudiant','Appeler Han Solo'),
('Denoeux','Chercheur','Appeler Han Solo'),
('Einstein','Scientifique','Appeler Han Solo');
INSERT INTO MembreContribueAProjet VALUES ('Vast','Etudiant','Utiliser les méthodes de l IA');


INSERT INTO InternePropose VALUES ('Carrez','Etudiant','Appeler Han Solo'),
('Denoeux','Chercheur','Utiliser les méthodes de l IA');

INSERT INTO Label VALUES ('IA'),
('Probabilité'),
('Espace'),
('Vaisseau'),
('Sabre Laser');

INSERT INTO LabelProposition VALUES ('Utiliser les méthodes de l IA','IA'),
('Utiliser les méthodes de l IA','Probabilité'),
('Appeler Han Solo','Espace'),
('Appeler Han Solo','Vaisseau'),
('Appeler Han Solo','Sabre Laser');

INSERT INTO EntiteDelivreLabel VALUES ('UTC','IA'),
('UTC','Probabilité'),
('NASA','Espace'),
('NASA','Vaisseau'),
('NASA','Sabre Laser');

INSERT INTO FinanceOrganisme VALUES ('UTC','Laboratoire CNRS1'),
('UTC','Laboratoire CNRS2'),
('UTC','Laboratoire CNRS3'),
('France','Laboratoire CNRS1'),
('France','Laboratoire CNRS2'),
('France','Laboratoire CNRS3'),
('NASA','SpaceX'),
('NASA','Star Wars'),
('ONU','Star Wars');