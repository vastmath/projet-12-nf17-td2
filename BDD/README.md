Ce dossier contient les différents éléments qui constitueront la base de données au format .txt :

- CREATE.txt : contient les codes permettant de créer les tables ainsi que les vues correspondant aux contraintes identifiées sur les relations entre les tables ou sur les tables elle-même
- CREATEJSON.txt : contient la seconde version du fichier CREATE.txt qui intègre le format JSON dans les tables et les contraintes
- INSERT.txt : contient les codes permettant d'insérer des données dans les tables précédemment créées
- INSERTJSON.txt : contient la deuxième version de INSERT.txt avec les données correspondant au fichier CREATEJSON.txt
- SELECT.txt : contient les vues correspondant aux recherches ou requêtes statistiques qui peuvent être effectuées sur la base de données
- LCD.txt : contient la gestion des droits pour les différents utilisateurs identifiés de la base de données