Ce dossier contient les différentes versions du MLD : 

- MLDv1 est la première version ayant été réalisée pour le livrable 2
- MLDv2 est la dexuième version du MLD. Elle contient les étapes de la normalisation du MLDv1 et a été réalisée dans le cadre du livrable 2
- MLDv3 est la troisième version du MLD. Elle est toujours normalisée mais contient cette fois des attributs au format JSON et a été réalisée dans le cadre du livrable 3.